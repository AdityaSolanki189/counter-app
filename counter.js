let value = 0;

let counterElement = document.getElementById("counter");
let warning = document.getElementById("warn");

document.getElementById("increment").addEventListener("click", function(){
    value++;
    counterElement.innerHTML = value;
})

document.getElementById("decrement").addEventListener("click", function(){
    value--;
    if(value < 0){
        warning.classList.remove("hide");
        counterElement.classList.add("shake");
        setTimeout(() => {
            value = 0;
            warning.classList.add("hide");
            counterElement.classList.remove("shake");
        }, 1500);
    }else{
        counterElement.innerHTML = value;    
    }
    // counterElement.innerHTML = value;
})

document.getElementById("reset").addEventListener("click", function(){
    value = 0;
    counterElement.innerHTML = value;
})